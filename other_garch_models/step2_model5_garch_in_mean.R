#####################################################################
## FM442: Summative Presentation
## Step 2, Model 5: GARCH_in_mean
## Author: Evan Chow
##
## Model description: this is a Garch-in-mean
## based on the NASDAQ-100 index. So, this is the other baseline
## model (along with CBOE's VXN index).
## https://stats.stackexchange.com/questions/96937/garch-m1-1-where-arma0-0-is-removed-in-r
## but with ARMA(1,1) (still some residual autocorrelation
## in the log returns)
## Focuses on close price.
#####################################################################

MODEL.NAME <- "GARCH_in_mean"
OUTPUT.DIR <- glue("step2_model5_garch_in_mean")

#####################################################################
### .rprofile imports and setup
set.seed(42)
suppressMessages(lapply(c("ggplot2", "dplyr", "lubridate", "ggthemes",
                          "glue", "data.table", "chron", "rugarch"),
                        require, character.only=T))
options(scipen=999) # disable scientific notation
hheader <- function(x) { print("###############"); print(glue("# {x}")); print("###############"); }
pprint <- function(x, newline=2) { cat(x); for (i in 1:newline) { cat("\n") } }
YYYYMMDD <- "%Y%m%d"; YYYYMMDD.DASH <- "%Y-%m%d"
FIG.WIDTH <- 13; FIG.HEIGHT <- 8

INPUT.DIR <- glue("step1_data2")
if (!dir.exists(OUTPUT.DIR)) { dir.create(OUTPUT.DIR, showWarnings = FALSE) }
prefix.output <- function(x) glue("{OUTPUT.DIR}/{x}")

# constants (should move to JSON)
START.YR <- 2010; END.YR <- 2020
START.DS <- ymd(glue("{START.YR}0101")); END.DS <- ymd(glue("{END.YR}1101"))

#####################################################################
### Read in cleaned data

fp.nasdaq.cleaned <- glue("{INPUT.DIR}/HistoricalQuotes_cleaned__{format(START.DS, YYYYMMDD)}_{format(END.DS, YYYYMMDD)}.csv")
if (!exists("df.nasdaq")) {
  colClasses <- c("Date", "numeric", "numeric", "numeric", "numeric", "logical", "logical", "logical")
  df.nasdaq <- data.frame(fread(fp.nasdaq.cleaned, colClasses = colClasses))
  df.nasdaq$Date <- ymd(df.nasdaq$Date)
}
num.cols <- colnames(df.nasdaq)[sapply(df.nasdaq, is.numeric)] # open, high, low, close

# for ggplot2: quarterly breaks
breaks_qtr = seq(from = min(df.nasdaq$Date), to = max(df.nasdaq$Date), by = "3 months")
labels_year = format(seq(from = min(df.nasdaq$Date), to = max(df.nasdaq$Date), by = "1 year"), "%Y")
labs = c(sapply(labels_year, function(x) { c(x, rep("", 3)) }))

# equivalent to is_trading_day == 1
df.nasdaq.trading_days <- df.nasdaq[complete.cases(df.nasdaq),]

#####################################################################
#####################################################################
#####################################################################
### Calculate GARCH(p,q) models
#####################################################################
#####################################################################
#####################################################################

require(tseries)

### Use log returns
### all that stuff from lecture. 
### - if don't make stationary then will blow up over time
nasdaq.returns <- df.nasdaq.trading_days$Close
nasdaq.returns <- nasdaq.returns / dplyr::lag(nasdaq.returns, 1)
nasdaq.returns <- log(nasdaq.returns)
qplot(df.nasdaq.trading_days$Date, nasdaq.returns) + geom_line() + 
  xlab("") + ylab("Log returns") + theme_economist_white()
# centered around 0 which is good - kinda stationary
acf(nasdaq.returns[!is.na(nasdaq.returns)])
pacf(nasdaq.returns[!is.na(nasdaq.returns)])
# some autocorrelation which is good (some volatilty clustering)

# only look at date #2 onward
nasdaq.returns <- nasdaq.returns[2:length(nasdaq.returns)]
df.nasdaq.trading_days <- df.nasdaq.trading_days[2:nrow(df.nasdaq.trading_days),]

#####################################################################
#####################################################################
#####################################################################
### Model and visualization - baseline example
#####################################################################
#####################################################################
#####################################################################

### Notes:
# note: log(x_{t+1}/x_t)*100 ~ (x_{t+1} / x_t - 1)*100
# For model annualised percentage:
# https://quant.stackexchange.com/questions/9190/garch1-1-prediction-in-r-basic-questions
# For VXN annualised percentage:
# https://www.cboe.com/publish/methodology-volatility/VXN_Methodology.pdf
# Note VXN is already annualised and in percentage terms, per the N_365/N_30 in the 
# formula and the coefficient 100.

# fit model
pq.order <- c(1,1)
garchspec <- ugarchspec(variance.model=list(model="sGARCH", garchOrder=pq.order), 
                        mean.model=list(armaOrder=c(1,1), archm=T, archpow=2, include.mean=F),
                        distribution.model = "std")
garch.pq <- ugarchfit(data=nasdaq.returns, spec=garchspec)

# annualise model estimatations as percentage
fitted.garch.pq <- as.numeric(sigma(garch.pq)) ** 2 # sigma^2
fitted.garch.pq <- sqrt(fitted.garch.pq) * sqrt(250) * 100
df.fitted.garch.pq <- data.frame(Date=df.nasdaq.trading_days$Date, Close=fitted.garch.pq)
model.name.pq <- glue("{MODEL.NAME}_{paste(c('p','q'),c(1,1), sep='',collapse='_')}")

# visualization
plt.garch.pq <- ggplot(df.fitted.garch.pq,
                       aes(x=Date, y=Close)) +
  geom_line() + theme_economist_white() + xlab("") + ylab("Close price") +
  ggtitle(glue("{model.name.pq} index, {START.DS} to {END.DS}")) +
  scale_x_date(labels = labs, breaks = breaks_qtr, name = "Year") +
  # scale_x_date(date_breaks = "1 year", date_minor_breaks = "3 month", date_labels = "%b-%y") + 
  theme(axis.text.x = element_text(angle = 45, vjust=0.25, size=14))
fp.viz <- prefix.output(glue("{model.name.pq}.png"))
ggsave(fp.viz, plt.garch.pq, width=FIG.WIDTH, height=FIG.HEIGHT, dpi=300)
# save data too
fp.viz.data <- prefix.output(glue("{model.name.pq}_fitted.csv"))
fwrite(df.fitted.garch.pq, fp.viz.data)

# compare annualised GARCH with VXN (already annualised), in % terms
df.vxn <- fread("step2_model1_vxn/VXN_fitted.csv") %>% mutate(Date=ymd(Date), group="VXN")

avg.nasdaq.return <- mean(df.nasdaq.trading_days$Close) # could better approx, e.g. MA(30)
ex.garch.vol.annualised.pct <- df.fitted.garch.pq[,"Close"] # already converted to sigma^2
ex.vxn.sd.annualised.pct <- (df.vxn[df.vxn$Date %in% df.nasdaq.trading_days$Date, "Close"])
df.ex <- data.frame(date=df.nasdaq.trading_days$Date,
                    garch=ex.garch.vol.annualised.pct, vxn=ex.vxn.sd.annualised.pct)
df.ex.melt <- reshape2::melt(df.ex, id.vars=c("date"))
plt.ex.melt <- ggplot(df.ex.melt, aes(x=date, y=value, group=variable, color=variable)) +
  geom_line() + xlab("") + ylab("Annualised volatility (%)") + theme_economist_white()
show(plt.ex.melt)

#####################################################################
#####################################################################
#####################################################################
### Model and visualization - for all specifications
###
###
#####################################################################
#####################################################################
#####################################################################

all.garch.models <- list()
all.garch.data <- list()
for (p in 1:5) { # basic (1,1) for now
  for (q in 1:5) {
    hheader(glue("Running {MODEL.NAME}({p},{q})"))
    res <- tryCatch({
      # model
      pq.order <- c(p,q)
      garchspec <- ugarchspec(variance.model=list(model="sGARCH", garchOrder=pq.order), 
                              mean.model=list(armaOrder=c(1,1), archm=T, archpow=2, include.mean=F),
                              distribution.model = "std")
      garch.pq <- ugarchfit(data=nasdaq.returns, spec=garchspec)
      
      # annualise model estimatations as percentage
      fitted.garch.pq <- as.numeric(sigma(garch.pq)) ** 2 # sigma^2
      fitted.garch.pq <- sqrt(fitted.garch.pq) * sqrt(250) * 100
      df.fitted.garch.pq <- data.frame(Date=df.nasdaq.trading_days$Date, Close=fitted.garch.pq)
      model.name.pq <- glue("{MODEL.NAME}_{paste(c('p','q'), sep='',collapse='_')}")
      
      # # visualization
      # plt.garch.pq <- ggplot(df.fitted.garch.pq,
      #                        aes(x=Date, y=Close)) +
      #   geom_line() + theme_economist_white() + xlab("") + ylab("Close price") +
      #   ggtitle(glue("{model.name.pq} index, {START.DS} to {END.DS}")) +
      #   scale_x_date(labels = labs, breaks = breaks_qtr, name = "Year") +
      #   # scale_x_date(date_breaks = "1 year", date_minor_breaks = "3 month", date_labels = "%b-%y") + 
      #   theme(axis.text.x = element_text(angle = 45, vjust=0.25, size=14))
      # fp.viz <- prefix.output(glue("{model.name.pq}.png"))
      # ggsave(fp.viz, plt.garch.pq, width=FIG.WIDTH, height=FIG.HEIGHT, dpi=300)
      # # save data too
      # fp.viz.data <- prefix.output(glue("{model.name.pq}_fitted.csv"))
      # fwrite(df.fitted.garch.pq, fp.viz.data)
      
      all.garch.models[[length(all.garch.models)+1]] <- list(
        model.name.pq=model.name.pq, p=p, q=q, model=garch.pq,
        AIC=infocriteria(garch.pq)['Akaike',], BIC=infocriteria(garch.pq)['Bayes',], data=df.fitted.garch.pq
      )
      all.garch.data[[length(all.garch.data)+1]] <- (
        df.fitted.garch.pq %>% mutate(group=glue("{MODEL.NAME}({p},{q})")))
      
      # ### save individual data too - only if need
      # fp.viz.data <- prefix.output(glue("{model.name.pq}_fitted.csv"))
      # fwrite(df.fitted.garch.pq, fp.viz.data)
    }, error=function(e) {
      print("Found error, continuing ...")
      print(e)
    }, finally = {
      print("continuing")
    })
  }
}
df.all.garch <- do.call(rbind, all.garch.data)
### overall, which have the best AIC and BIC (both)
### It seems AIC and BIC both return roughly the same best models (for GARCH)
### so I will just use AICs.
AICs <- unlist(sapply(all.garch.models, function(x) x['AIC']))
best.AICs.ixs <- sort(AICs, index.return=TRUE)$ix
# BICs <- unlist(sapply(all.garch.models, function(x) x['BIC']))
# best.BICs.ixs <- sort(AICs, index.return=TRUE)$ix
### combine both
best.AIC_and_BIC.ixs <- sort(AICs, index.return=TRUE)$ix[1]
best.garch.models <- all.garch.models[best.AIC_and_BIC.ixs]
best.garch.data <- all.garch.data[best.AIC_and_BIC.ixs]
df.best.all.garch <- do.call(rbind, best.garch.data) # selected three best based on AIC and BIC both
df.plt <- df.best.all.garch
# save data before appending VXN
fp.viz.data <- prefix.output(glue("{MODEL.NAME}_fitted.csv"))
# add VaR column
df.nasdaq <- data.frame(fread("step2_model0_nasdaq/NASDAQ_log_returns_fitted.csv")) %>%
  mutate(Date=ymd(Date))
df.plt <- merge(df.plt, df.nasdaq, by="Date", all.x=TRUE, suffix = c("", "_log_ret"))
df.plt$Close.VaR_5 <- with(df.plt, qnorm(0.05) * (Close / (sqrt(252) * 100)))
df.plt$Close.VaR_1 <- with(df.plt, qnorm(0.01) * (Close / (sqrt(252) * 100)))
# ggplot(melt(df.plt[,c("Date", "Close_log_ret", "Close.VaR_5")], id.vars=c("Date")),
#     aes(x=Date, y=value, group=variable, color=variable)) + geom_line()
fwrite(df.plt, fp.viz.data)

# plot VXN as well for comparison
df.vxn <- fread("step2_model1_vxn/VXN_fitted.csv") %>% mutate(Date=ymd(Date), group="VXN")
df.vxn <- df.vxn[df.vxn$Date %in% df.best.all.garch$Date,]
df.plt <- dplyr::bind_rows(df.plt, df.vxn)

# plot models and VXN
plt.all.garch <- ggplot(df.plt, aes(x=Date, y=Close, group=group, color=group)) +
  geom_line() + xlab("") + ylab("Annualized volatility (%)") +
  ggtitle(glue("{MODEL.NAME}(p,q) for NASDAQ, {START.DS} to {END.DS}, with VXN for comparison")) +
  theme_economist_white() +
  scale_x_date(labels = labs, breaks = breaks_qtr, name = "Year") +
  theme(axis.text.x = element_text(angle = 45, vjust=0.25, size=14),
        legend.position="right", legend.title=element_blank())
fp.viz <- prefix.output(glue("{MODEL.NAME}.png"))
ggsave(fp.viz, plt.all.garch, width=FIG.WIDTH, height=FIG.HEIGHT, dpi=300)




