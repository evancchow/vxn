### Volatility indices with the NASDAQ

(VXN)

---

Added slides etc. Don't think I'll have time to get to RNN.

https://cran.r-project.org/web/packages/rnn/vignettes/rnn.html
http://rstudio-pubs-static.s3.amazonaws.com/248919_2bf5b65d86a645af8836794229f6bbaa.html
https://cran.r-project.org/web/packages/rnn/rnn.pdf

* 12/1 - meeting TODOs
    Last bit of work: double-check VaR graphs against reading in the models etc. Can probably skip MA since have EWMA.
    
* 12/1 - TODOs
    - [DONE] stochvol + basic backtesting
    - [DONE] redo MA with non-EWMA approximation
    - another volatility model + basic backtesting
    - another volatility model + basic backtesting
    - stochvol: rolling forecast backtesting like ugarchroll
    
note: rugarch estimates and uses alpha1 = 1-lambda:
http://www.unstarched.net/r-examples/rugarch/the-ewma-model/
I've added this correction for MA, EWMA.

* 11/30 - TODOs
    - [backlog] add tnorm (instead of qnorm) for VAR@risk
    - [DONE] add better model validation for GARCH, then rest of models
    - [DONE] reimplement all models (MA, EWMA) except VXN in the rugarch framework
      for unified information, diagnostics, Backtesting
    - [DONE] VXN: add diagnostics (backtesting for instance)
      - >>> Under how ugarchroll backtesting procedure works, then implement for VXN.
      - refitting every N iterations = every 100 rolling windows, re-estimate
      - should be faster than parallel, since VXN already calculated / 
        not grid-searching over many parameters.
      - it seems basic idea is, with t_start=100 or however long to wait 
        until start generating forecasts:
      
      for t in t_start:t_end
          curr_window = [t-window_size, t]
          # I think this is correct because if you plot(ugarchroll.fit) and
          # pick option 5, you get the number of refits = (num. of data points -
          # num. of burn-in samples) / (num. times to refit).
          if t % 100: reestimate model on curr_window
          forecast t+1
          if violates VaR: exceedances += 1
          num_checks += 1
      compare exceedances / num_checks.
      
      so this is great, because it just means you can implement the same
      backtesting method for VXN for t=(# burn in samples+1):(last time period),
      e.g. t=500:2000. note, will want to use that same consistent time period
      for backtesting VXN, other models. Other models you could similarly 
      refit as.
      
      On whether window just expands or slides along with the same length:
      https://rdrr.io/cran/rugarch/man/ugarchroll-methods.html
      http://www.unstarched.net/r-examples/rugarch/a-short-introduction-to-the-rugarch-package/
      https://www.r-bloggers.com/2017/11/formal-ways-to-compare-forecasting-models-rolling-windows/
      
      for sliding ("moving") I think it always still increments by 1,
      because if I change refit.every argument, it still yields the same
      backtest length (2000) and the same expected exceed (2000*0.05=100),
      the former corresponding to our number of observations here. of course,
      the number of refits changes with refit.every (to 2000/refit.every
      refits), but that doesn't nullify the fact that we still estimate 
      at every step (for those 2000 observations post-burn-in).
      
      note that for VXN, there is no concept of a "forecast" so this 
      simply reduces to checking the number of violations across that
      VXN time series (of course, ignoring the "burn-in" period).
      
      for stochvol and other models, can implement with that "forecasting"
      approach of course, similar to the ugarchroll method.
      
    - implement other models like stochvol, using same backtesting code
      as you used for VXN
    - redo MA with non-EWMA approximation later if time
    - other models
    
Model parameter ranges:
    - GARCH(p,q): from (1,1) to (3,3)
    - ARCH(p): from 1 to 14
    - EWMA(lambda): estimates lambda via MLE (note have to convert from 1-lambda)
    - MA(p): from 1 to 14 (approx w/EWMA - some evaluation things may not exist)
    
EWMA special case of GARCH (iGARCH):
http://faculty.baruch.cuny.edu/smanzan/FINMETRICS/_book/volatility-models.html
http://www.unstarched.net/r-examples/rugarch/the-ewma-model/

I think MA will be a special case of this where all the GARCH terms (betas) equal zero
and the other ARCH coefficients still add up to 1. (mean in either MA, EWMA will be zero).

Will have to reimplement rugarch's historical backtesting (https://academic.oup.com/jfec/article-abstract/2/1/84/960714?redirectedFrom=PDF) for the VXN historical series. Will have to do this for other models (stochvol, etc.) too if do.

GARCH historical backtesting:
    - https://shishirshakya.blogspot.com/2015/07/garch-model-estimation-backtesting-risk.html
    - http://www.unstarched.net/r-examples/rugarch/a-short-introduction-to-the-rugarch-package/
    - Christoffsen test: https://erm.ncsu.edu/az/erm/i/chan/library/Christoffersen-Pelletier-Backtesting.pdf

May need to expand y-axis (or use log) so can see differences
between different volatility models.

* Interesting volatility models:
    - https://www.tandfonline.com/doi/pdf/10.1080/07350015.2017.1415910?casa_token=oKfC_59bQZUAAAAA:zWZvlZ8RiN8rXoDlZFdn_wdMeDxB3-ldk5QFl0KV1Yt-186jlU0vmnwcsY-4LHHmtR2OrDWJN4iE
    - https://rpubs.com/simaan84/ML_Vol
    - Neural stochastic volatility model: https://openreview.net/pdf?id=B1IzH7cxl

ML: less interpretable so may be less appropriate, BUT could still do 
and just mention that fact.

* 11/28 - TODOs
    - resize plots so consistent with presentation plots
    - add VaR in step3
    - add volatility and VaR to presentation slides
    - finish up stochvol
    - other models
    - add tnorm (instead of qnorm) for VAR@risk
  
Presentation meeting notes:
    - 
    - BIC, time series CV
    - current volatility methods to compare and contrast (state-of-the-art)
    - practice presentation
    
* 11/25 - TODOs
    - GARCH models from lecture
    - SVR-GARCH (ML / GARCH)
    - SVM - radial https://rpubs.com/simaan84/ML_Vol
    - standard multilayer perceptron - https://core.ac.uk/download/pdf/82397719.pdf
    - deep learning?
    - markov model thing (regime-switching)

* 11/23 - TODOs
    - figuring out if can get ^NDX options data (note, options on the index) so
    can reconstruct VXN-like indices: https://datashop.cboe.com/option-quotes
    with "^NDX" in the text box.
    - other GARCH models
    - machine learning volatility (less transparent)
    - get NASDAQ-100 (NDX) options so can create VIX-like index
    - plot all models on one plot (eventually) - have best (e.g. AIC/BIC-minimizing)
    model per model family, e.g. VIX, one GARCH model, one neural network model,
    one VIX variant, etc.

- 11/22 - TODOs
    - [DONE] plot the NASDAQ by itself (log returns) to see potential volatility periods ("step2_model0")
    - [DONE] align y-axes for GARCH and VIX, as in lecture
      - [DONE] figure out what the proper annualisation (if any) should be for VXN
      - [DONE] switch to ugarch [https://rpubs.com/pjozefek/573323]
    - other GARCH models
    - machine learning volatility (less transparent)
    - get NASDAQ-100 (NDX) options so can create VIX-like index
    - plot all models on one plot (eventually) - have best (e.g. AIC/BIC-minimizing)
    model per model family, e.g. VIX, one GARCH model, one neural network model,
    one VIX variant, etc.