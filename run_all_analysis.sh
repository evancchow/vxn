Rscript step2_model0_nasdaq.R &&
Rscript step2_model1_vxn.R &&
Rscript step2_model2_garch.R &&
Rscript step2_model3_arch.R &&
Rscript step2_model4_ewma.R &&
Rscript step2_model5_ma.R &&
Rscript step2_model6_stochvol.R &&
Rscript step3_plot_all_fitted_models.R &&
echo "Done."