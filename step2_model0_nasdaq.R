#####################################################################
## FM442: Summative Presentation
## Step 2, Model 0: NASDAQ
## Author: Evan Chow
##
## Model description: this just visualizes the raw NASDAQ data
## (and so is "Model 0").
## Focuses on close price.
#####################################################################

MODEL.NAME <- "NASDAQ"

#####################################################################
### .rprofile imports and setup
set.seed(42)
suppressMessages(lapply(c("ggplot2", "dplyr", "lubridate", "ggthemes",
                          "glue", "data.table", "chron"),
                        require, character.only=T))
options(scipen=999) # disable scientific notation
hheader <- function(x) { print("###############"); print(glue("# x")); print("###############"); }
pprint <- function(x, newline=2) { cat(x); for (i in 1:newline) { cat("\n") } }
YYYYMMDD <- "%Y%m%d"; YYYYMMDD.DASH <- "%Y-%m%d"
FIG.WIDTH <- 13; FIG.HEIGHT <- 8

INPUT.DIR <- glue("step1_data2")
OUTPUT.DIR <- glue("step2_model0_nasdaq")
if (!dir.exists(OUTPUT.DIR)) { dir.create(OUTPUT.DIR, showWarnings = FALSE) }
prefix.output <- function(x) glue("{OUTPUT.DIR}/{x}")

# constants (should move to JSON)
START.YR <- 2010; END.YR <- 2020
START.DS <- ymd(glue("{START.YR}0101")); END.DS <- ymd(glue("{END.YR}1101"))

#####################################################################
### Read in cleaned data

fp.nasdaq.cleaned <- glue("{INPUT.DIR}/HistoricalQuotes_cleaned__{format(START.DS, YYYYMMDD)}_{format(END.DS, YYYYMMDD)}.csv")
if (!exists("df.nasdaq")) {
  colClasses <- c("Date", "numeric", "numeric", "numeric", "numeric", "logical", "logical", "logical")
  df.nasdaq <- data.frame(fread(fp.nasdaq.cleaned, colClasses = colClasses))
  df.nasdaq$Date <- ymd(df.nasdaq$Date)
}
num.cols <- colnames(df.nasdaq)[sapply(df.nasdaq, is.numeric)] # open, high, low, close

# for ggplot2: quarterly breaks
breaks_qtr = seq(from = min(df.nasdaq$Date), to = max(df.nasdaq$Date), by = "3 months")
labels_year = format(seq(from = min(df.nasdaq$Date), to = max(df.nasdaq$Date), by = "1 year"), "%Y")
labs = c(sapply(labels_year, function(x) { c(x, rep("", 3)) }))

# equivalent to is_trading_day == 1
df.nasdaq.trading_days <- df.nasdaq[complete.cases(df.nasdaq),]

# convert into log returns


#####################################################################
### Model and visualization
plt.nasdaq <- ggplot(df.nasdaq.trading_days,
                  aes(x=Date, y=Close)) + 
  geom_line() + theme_economist_white() + xlab("") + ylab("Close price") +
  ggtitle(glue("{MODEL.NAME} index, {START.DS} to {END.DS}")) +
  scale_x_date(labels = labs, breaks = breaks_qtr, name = "Year") +
  # scale_x_date(date_breaks = "1 year", date_minor_breaks = "3 month", date_labels = "%b-%y") + 
  theme(axis.text.x = element_text(angle = 45, vjust=0.25, size=14))
fp.viz <- prefix.output(glue("{MODEL.NAME}.png"))
ggsave(fp.viz, plt.nasdaq, width=FIG.WIDTH, height=FIG.HEIGHT, dpi=300)

# save data too
fp.viz.data <- prefix.output(glue("{MODEL.NAME}_fitted.csv"))
fwrite(df.nasdaq.trading_days[, c("Date", "Close")], fp.viz.data)

#####################################################################
### Examine log returns

nasdaq.returns <- df.nasdaq.trading_days$Close
nasdaq.returns <- nasdaq.returns / dplyr::lag(nasdaq.returns, 1)
nasdaq.returns <- log(nasdaq.returns)
plt.log.nasdaq <- qplot(df.nasdaq.trading_days$Date, nasdaq.returns) + geom_line() + 
  xlab("") + ylab("Log returns") + theme_economist_white()
# centered around 0 which is good - kinda stationary
# acf(nasdaq.returns[!is.na(nasdaq.returns)])
# pacf(nasdaq.returns[!is.na(nasdaq.returns)])
# # some autocorrelation which is good (some volatilty clustering)
fp.viz <- prefix.output(glue("{MODEL.NAME}_log_returns.png"))
ggsave(fp.viz, plt.log.nasdaq, width=FIG.WIDTH, height=FIG.HEIGHT, dpi=300)
# save data too
fp.viz.data <- prefix.output(glue("{MODEL.NAME}_log_returns_fitted.csv"))
fwrite(data.frame(Date=df.nasdaq.trading_days$Date,Close=nasdaq.returns), fp.viz.data)


